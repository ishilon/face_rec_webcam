import pkg_resources
import os
import PIL.Image as pimg
import dlib
import numpy as np

face_detector = dlib.get_frontal_face_detector()

models_dir = pkg_resources.resource_filename( 'face_rec', 'models/' )

predictor_68_point_model = os.path.join(models_dir, 'shape_predictor_68_face_landmarks.dat')
try:
    pose_predictor_68_point = dlib.shape_predictor(predictor_68_point_model)
except RuntimeError:
    print('File not found!')

predictor_5_point_model = os.path.join(models_dir, 'shape_predictor_5_face_landmarks.dat')
try:
    pose_predictor_5_point = dlib.shape_predictor(predictor_5_point_model)
except RuntimeError:
    print('File not found!')

try:
    cnn_face_detection_model = os.path.join(models_dir, 'mmod_human_face_detector.dat')
except RuntimeError:
    print('File not found!')

try:
    cnn_face_detector = dlib.cnn_face_detection_model_v1(cnn_face_detection_model)
except RuntimeError:
    print('File not found!')

face_recognition_model = os.path.join(models_dir, 'dlib_face_recognition_resnet_model_v1.dat')
face_encoder = dlib.face_recognition_model_v1(face_recognition_model)

pose_models_list = ['5_points', '68_points']


def load_image(file, mode='RGB'):
    """Load an image file and return it as numpy array

    Arguments:
        file {string} -- A filename (string), pathlib.Path object or a file object.
        The file object must implement read(), seek(), and tell() methods, and be opened in binary mode.

    Keyword Arguments:
        mode {str} -- Pillow format name to convert the image to (default: {'RGB'}). See the Pillow docs for more information.
        http://pillow.readthedocs.io/en/5.1.x/reference/Image.html

    Returns:
        array -- A numpy array representation of the image
    """
    im = pimg.open(file).convert(mode)  # Open to image and convert according to the mode argument
    return np.array(im)


def _rect_to_sides(rect):
    """Convert a dlib 'rectangle' object to a plain tuple with the order (left, top, right, bottom) order

    Arguments:
        rect {rectangle} -- a dlib 'rectangle' object

    Returns:
        tuple -- A tuple representation of the rectangle in the order (left, top, right, bottom)
    """
    return rect.left(), rect.top(), rect.right(), rect.bottom()


def _sides_to_rect(sides):
    """Convert a sides tuple, in order (left, top, right, bottom), to a dlib 'rectangle' object

    Arguments:
        sides {tuple} -- A tuple of the rectangle sides

    Returns:
        rectangle -- a dlib rectangle object
    """
    return dlib.rectangle(sides[0], sides[1], sides[2], sides[3])


def _trim_sides_to_image_bounds(sides, image_shape):
    """Trim the rectangle sides tuple ( with the order (left, top, right, bottom) ) to the boundaries of the image

    Arguments:
        sides {tuple} -- A tuple representation of a dlib 'rectangle' object. Must have the order (left, top, right, bottom)
        image_shape {tuple} -- A numpy shape tuple of the image array

    Returns:
        tuple -- A tuple representation of the 'rectangle' object that is contained within the image boundaries
    """
    return max( sides[0], 0 ), max( sides[1], 0 ), min( sides[2], image_shape[1] ), min( sides[3], image_shape[0] )


def _raw_face_locations(img, upsample=1, model='hog'):
    """Get a list of bounding boxes of detected faces in an image

    Arguments:
        img {array} -- A numpy array representation of the image

    Keyword Arguments:
        upsample {int} -- [description] (default: {1})
        model {str} -- [description] (default: {'hog'})

    Returns:
        list -- A list of dlib 'rectangle' objects of found face locations
    """
    if model == 'cnn':
        return cnn_face_detector(img, upsample)
    else:
        return face_detector(img, upsample)


def _batch_raw_face_locations(images, upsample=1, batch_size=128):
    """Get a 2D tuple of dlib 'rectangle' objects, located around detected faces, in an image - using the CNN face detector

    Arguments:
        images {list} -- A list of numpy arrays, each represents an image

    Keyword Arguments:
        upsample {int} -- [description] (default: {1})
        batch_size {int} -- The size of the batch (default: {128})

    Returns:
        list -- A list of 'rectangle' objects of detected face locations
    """
    return cnn_face_detector( images, upsample, batch_size=batch_size )


def face_locations(img, upsample=1, model='hog'):
    """Get a list of bounding boxes of detected faces in an image

    Arguments:
        img {array} -- A numpy array representation of the image

    Keyword Arguments:
        upsample {int} -- The number of times to upsample the image while looking for faces. Higher numbers find smaller faces (default: {1})
        model {str} -- The face detection model to be applied. (default: {'hog'})

    Returns:
        list -- A list of tuples of found face locations, each with the order (left, top, right, bottom)
    """
    if model == 'cnn':
        return [ _trim_sides_to_image_bounds( _rect_to_sides(face.rect), img.shape ) for face in _raw_face_locations(img, model='cnn') ]
    else:
        return [ _trim_sides_to_image_bounds( _rect_to_sides(face), img.shape ) for face in _raw_face_locations(img) ]


def batch_face_locations(images, upsample=1, batch_size=128):
    """Get a 2D array of bounding boxes of detected faces, using the CNN detector (i.e. using CUDA)

    Arguments:
        images {list} -- A list of numpy arrays, representing images

    Keyword Arguments:
        upsample {int} -- The number of times to upsample the image while looking for faces. Higher numbers find smaller faces (default: {1})
        batch_size {int} -- The size of the batch (default: {128})

    Returns:
        list -- A list of tuples of detected face locations in the order (left, top, right, bottom)
    """
    def convert_cnn_detections_to_sides(detections):
        return [ _trim_sides_to_image_bounds( _rect_to_sides(face.rect), images[0].shape ) for face in detections ]

    batch_raw_detections = _batch_raw_face_locations( images, upsample, batch_size )

    return [convert_cnn_detections_to_sides(detections) for detections in batch_raw_detections]  # list(map(convert_cnn_detections_to_sides, batch_raw_detections))


def _raw_face_landmarks(face_img, face_locations=None, model='68_points'):
    """[summary]

    Arguments:
        face_img {[type]} -- [description]

    Keyword Arguments:
        face_locations {[type]} -- [description] (default: {None})
        model {str} -- [description] (default: {'68_points'})

    Returns:
        [type] -- [description]
    """
    assert model in pose_models_list, 'Please check your model name!'
    if not face_locations:
        face_locations = _raw_face_locations(face_img)
    else:
        face_locations = [_sides_to_rect(face_location) for face_location in face_locations]

    if model == '5_points':
        pose_predictor = pose_predictor_5_point
    elif model == '68_points':
        pose_predictor = pose_predictor_68_point

    return [ pose_predictor(face_img, face_location) for face_location in face_locations ]


def face_landmarks(face_img, face_locations=None):
    """Given an image, return a dictionary of face feature locations (eyes, nose, etc.) for each face in the image

    Arguments:
        face_img {[type]} -- An image to extract face features in

    Keyword Arguments:
        face_locations {[type]} -- A list of face known face locations in the image (default: {None})

    Returns:
        list -- A list of dicts with face features for each found found face in the image
    """
    landmarks = _raw_face_landmarks(face_img, face_locations)
    landmarks_tuples = [ [ (p.x, p.y) for p in landmark.parts() ] for landmark in landmarks ]

    # For a definition of each point index, see https://cdn-images-1.medium.com/max/1600/1*AbEg31EgkbXSQehuNJBlWg.png
    return [{
        "chin":
        points[0:17],
        "left_eyebrow":
        points[17:22],
        "right_eyebrow":
        points[22:27],
        "nose_bridge":
        points[27:31],
        "nose_tip":
        points[31:36],
        "left_eye":
        points[36:42],
        "right_eye":
        points[42:48],
        "top_lip":
        points[48:55] + [points[64]] + [points[63]] + [points[62]] +
        [points[61]] + [points[60]],
        "bottom_lip":
        points[54:60] + [points[48]] + [points[60]] + [points[67]] +
        [points[66]] + [points[65]] + [points[64]]
    } for points in landmarks_tuples]


def get_face_encodings(face_img, known_face_locations=None, num_jitters=1):
    """Given an image, return the 128 embedding vector for each face in the image

    Arguments:
        face_img {[type]} -- An image containing faces

    Keyword Arguments:
        known_face_locations {[type]} -- if known, the list of bounding boxes of already known faces in the image (default: {None})
        num_jitters {int} -- How many times to re-sample the face when calculating encoding. Higher is more accurate, but slower (i.e. 100 is 100x slower) (default: {1})

    Returns:
        list -- A list of 128D face embedding vectors, one for each face found in the image
    """
    raw_landmarks = _raw_face_landmarks(face_img, known_face_locations, model='5_points')

    return [ np.array( face_encoder.compute_face_descriptor( face_img, raw_landmark_set, num_jitters ) ) for raw_landmark_set in raw_landmarks ]


def embd_distance(face_encodings, vec_to_compare):
    """This function gets the Euclidean distance for each entry in a list of face vector embeddings compared to a known embedded face vector

    Arguments:
        face_encodings {list} -- a list of face vector embeddings
        vec_to_compare {array} -- An array of face embedding vector to compare to

    Returns:
        array -- A numpy array with the distance for each of the faces in the same order as the face_encodings list
    """

    if face_encodings:
        return np.linalg.norm(face_encodings - vec_to_compare, axis=1)
    else:
        return np.empty(0)


def compare_faces(face_encodings, embedding_vec_to_compare, tolerance=0.52):
    """Compare a list of face ebedding vectors against a candidate vector to see if they match

    Arguments:
        face_encodings {list} -- A list of embedding vectors for faces found in an image
        embedding_vec_to_compare {array} -- An embedding vector to compare to the found faces in the image

    Keyword Arguments:
        tolerance {float} -- The maximum allowed distance in the embedding phase-space between the face in the image to the known embedding vector (default: {0.6})

    Returns:
        list -- A list of bools indicating which face_encodings match the embedding vector to compare
    """

    return list(embd_distance(face_encodings, embedding_vec_to_compare) <= tolerance)


def get_num_cpus(number_of_cpus):
    max_cpus = os.cpu_count()  # the total number of cpu cores
    if number_of_cpus == -1 or number_of_cpus > max_cpus:
        processes = None
    else:
        processes = number_of_cpus

    return processes

# css[0] = sides[1] top
# css[1] = sides[2] right
# css[2] = sides[3] bottom
# css[3] = sides[0] left
