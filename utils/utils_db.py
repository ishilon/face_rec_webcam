import pymongo
from bson.binary import Binary as bsbin
import numpy as np


def arr_to_bytes(vec, dtype):
    """convert a numpy array to bytes object

    Arguments:
        vec {array} -- A numpy array
        dtype {np.dtype} -- The numpy data type used for conversion

    Returns:
        bytes -- A bytes object representation of the array
    """
    return vec.astype(dtype).tobytes()


def bytes_to_bson_binary(bytes):
    return bsbin(bytes)


def dbvec_to_array(user, dtype):
    """Retrieve the numpy array representation of the bytes object

    Arguments:
        user {dict} -- A row from the user db collection
        dtype {dtype} -- The numpy data type

    Returns:
        array -- The numpy array representation of the bytes object
    """
    return np.frombuffer(user['vec'], dtype=dtype)


def create_user(vec, dtype, first_name, last_name):
    vec_bytes = arr_to_bytes(vec, dtype)
    vec_bin = bytes_to_bson_binary(vec_bytes)
    return {'first_name': first_name, 'last_name': last_name, 'vec': vec_bin}


def add_user_to_collection(coll, user):
    coll.insert_one(user)
