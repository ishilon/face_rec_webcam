import pkg_resources
from os.path import join
import cv2
import numpy as np
import json
import utils.utils_db as dbu
import utils.utils_gen as utils
import my_db.mongo as mongou
import my_utils.gen as genu

# This is a demo of running face recognition on live video from the webcam. It includes some basic performance tweaks to make things run a lot faster:
#   1. Process each video frame at 1/4 resolution (though still display it at full resolution)
#   2. Only detect faces in every other frame of video.
# This script requires OpenCV (the `cv2` library) to be installed only to read from your webcam.
# Run the 'mongod' command before running this application
# Run with python3 main.py face_rec_webcam
# Don't forget to hit the 'r' button to add unknown faces


def bounding_box_with_text(fx, fy, left, top, right, bottom, frame, text):
    # Scale back up face locations since the frame we detected in was scaled to 1/4 size
    left *= int(1 / fy)
    top *= int(1 / fx)
    right *= int(1 / fy)
    bottom *= int(1 / fx)

    # Draw a box around the face
    cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

    # Draw a label with a text below the face
    cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
    font = cv2.FONT_HERSHEY_DUPLEX
    cv2.putText(frame, text, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)


# imgs_dir = pkg_resources.resource_filename('face_rec', 'images/')
# Load a sample picture and learn how to recognize it.
# idan_image = face_rec.load_image(join(imgs_dir, "idan.jpg"))
# idan_face_encoding = face_rec.get_face_encodings(idan_image)[0]
# idan_face_encoding = np.ones_like(idan_face_encoding)
# barack_image = face_rec.load_image(join(imgs_dir, "obama-480p.jpg"))
# barack_encoding = face_rec.get_face_encodings(barack_image)[0]
# np.save( join(imgs_dir, 'idan_vec.npy'), idan_face_encoding )
# idan_face_bytes = dbu.arr_to_bytes(idan_face_encoding, dtype=np.float64)

# data = {'name': 'Idan', 'vec': idan_face_encoding}
# datab = {'name': 'Idan', 'vec': idan_face_bytes}

# print(data)
# print(datab)

host = 'localhost'
port = 27017
db_name = 'face_rec'
coll32_str = 'test_coll_32'
coll64_str = 'test_coll_64'

mon = mongou.Mongo(host, port, db_name)
coll64 = mon.get_collection(coll64_str)

known_face_encodings = []
known_face_names = []
print('Reading DB...')
for user in coll64.find():
    known_face_encodings.append(dbu.dbvec_to_array(user, np.float64))
    known_face_names.append(user['first_name'])

# print(idan_face_encoding)
# print(np.max(idan_face_encoding))
# print(idan_face_bytes)
#import sys
#sys.exit()
# Create arrays of known face encodings and their names
#known_face_encodings = [barack_encoding]
#known_face_names = ['barack']

# Initialize some variables
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

fx = 0.5
fy = 0.5
assert fx < 1, 'fx should be smaller than 1'
assert fy < 1, 'fy should be smaller than 1'
win_name = 'webcam'
interface_win_name = 'face_to_add'

# Get a reference to webcam #0 (the default one)
print('Turning on webcam')
vid_capture = cv2.VideoCapture(0)
# Check if the webcam is opened correctly
if not vid_capture.isOpened():
    raise IOError("Cannot open webcam")

while vid_capture.isOpened():
    # Grab a single frame of video
    ret, frame = vid_capture.read()

    if ret is True:
        # Only process every other frame of video to save time
        if process_this_frame:
            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(frame, (0, 0), fx=fx, fy=fy)

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_small_frame = small_frame[:, :, ::-1]
            # Find all the faces and face encodings in the current frame of video
            face_locations = utils.face_locations(rgb_small_frame, model='hog')
            face_encodings = utils.get_face_encodings( rgb_small_frame, face_locations )

            face_names = []
            for i, face_encoding in enumerate(face_encodings):
                # See if the face is a match for the known face(s)
                matches = utils.compare_faces(known_face_encodings, face_encoding)
                name = "Unknown"

                # If a match was found in known_face_encodings, just use the first one.
                if True in matches:
                    first_match_index = matches.index(True)
                    name = known_face_names[first_match_index]

                if name == 'Unknown':
                    # Hit 'r' on the keyboard for database processing!
                    if cv2.waitKey(10) & 0xFF == ord('r'):
                        print('Processing image :-)')
                        loc = face_locations[i]
                        bounding_box_with_text(fx, fy, loc[0], loc[1], loc[2], loc[3], frame, 'Add?')
                        embd_to_process = face_encoding
                        # Display the resulting image
                        cv2.imshow(interface_win_name, frame)
                        cv2.waitKey()
                        add_user = genu.interaction_point('Would you like to add the bounded face to the DB?', exit=False, timeout=30)
                        if add_user:
                            name1 = input('Please enter your first name: ')
                            name2 = input('Please enter your last name: ')
                            # input -> please confirm
                            print('Adding face to DB!')
                            user_entry = dbu.create_user(embd_to_process, np.float64, name1, name2)
                            dbu.add_user_to_collection(coll64, user_entry)
                            print('User added :)')
                            known_face_encodings.append(embd_to_process)
                            known_face_names.append(name1)
                        else:
                            print('These face will remain anonymous!')
                            known_face_encodings.append(embd_to_process)
                            known_face_names.append('Anon')
                        cv2.destroyWindow(interface_win_name)
                face_names.append(name)

        process_this_frame = not process_this_frame

        # Display the results
        for (left, top, right, bottom), name in zip(face_locations, face_names):
            bounding_box_with_text(fx, fy, left, top, right, bottom, frame, name)

        # Display the resulting image
        cv2.imshow(win_name, frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(10) & 0xFF == ord('q'):
        print('Quitting!')
        break

# Release handle to the webcam
vid_capture.release()
cv2.destroyAllWindows()
mon.close_connection()
