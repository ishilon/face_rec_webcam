import click
import importlib
import scripts


@click.command()
@click.argument('task')
def main(task):
    task_module = 'scripts.' + task
    task = importlib.import_module(task_module)

if __name__ == "__main__":
    main()
